# Flectra Community / connector-cmis

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[cmis](cmis/) | 2.0.1.0.1| Connect Odoo with a CMIS server


